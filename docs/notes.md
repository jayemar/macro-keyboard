
## Parts
### CAD Drawings
[Macro Keypad from Thingiverse](https://www.thingiverse.com/thing:4642392)
[Macro Keypad build guide](https://imgur.com/gallery/6rOyqDD)
#### Adjustments to Make
- Needed to break out inner rings to allow rotary encoder to fit through
- The rotary encoders that I had on hand did not fit into the available inset; the inset is 12.7mm x 12.7mm, but my encoder is roughly 12.4mm x 13.4mm

### 3-D Printing
Shapeways order #3888837 on May 23rd, 2021

## References
### Rotary Encoder
**NOTE:** This did NOT work as well as hoped.  I ended up writing my own very simple implementation based on the rising interrupt for a single pin and comparing to the other pin of the encoder.

[Rotary encoder using Arduino](https://lastminuteengineers.com/rotary-encoder-arduino-tutorial/)

### Seeduino XIAO
[Seeduino XIAO](https://wiki.seeedstudio.com/Seeeduino-XIAO/)

![XIAO Pinout](Seeeduino_XIAO_Pinout-1.png)
