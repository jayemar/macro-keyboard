#include "Arduino.h"
/* #include "Keyboard.h" */
#include "HID-Project.h"

const int ledPin = 13;

unsigned int eventInterval = 50;  // minimum time between events in millis

// Define scroll encoder (middle)
const int scrollClk = 5;  // clock pin (CLK) in for rotary encoder #1
const int scrollDet = 6;  // detent pin (DT) for rotary encoder #1
unsigned long lastScrollEvt = 0;  // time (millis) of late rotary 1 event; used for de-bouncing

// Define volume encoder (right)
const int volClk = 8;   // clock pin (CLK) in for rotary encoder #2
const int volDet = 10;  // detent pin (DT) for rotary encoder #2
unsigned long lastVolEvt = 0;  // time (millis) of late rotary 2 event; used for de-bouncing

void pressSingleKey(char key) {
    Keyboard.press(key);
    delay(150);
    Keyboard.releaseAll();
}

void volumeUp() {
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press(KEY_LEFT_ALT);
    Keyboard.press(KEY_LEFT_SHIFT);
    Keyboard.press(KEY_UP_ARROW);
    delay(150);
    Keyboard.releaseAll();
}

void volumeDown() {
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press(KEY_LEFT_ALT);
    Keyboard.press(KEY_LEFT_SHIFT);
    Keyboard.press(KEY_DOWN_ARROW);
    delay(150);
    Keyboard.releaseAll();
}

void toggleMute() {
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press(KEY_LEFT_ALT);
    Keyboard.press(KEY_LEFT_SHIFT);
    Keyboard.press(' ');
    delay(250);
    Keyboard.releaseAll();
}

void undo() {
    Keyboard.press(KEY_LEFT_CTRL);
    Keyboard.press('z');
    delay(250);
    Keyboard.releaseAll();
}

void scrollDown() {
    Keyboard.press(KEY_DOWN_ARROW);
    delay(150);
    Keyboard.releaseAll();
}

void scrollUp() {
    Keyboard.press(KEY_UP_ARROW);
    delay(150);
    Keyboard.releaseAll();
}

/**
 * Increase/decrease system volume based on rotary encoder
 */
void updateVolume() {
    if (millis() - lastVolEvt > eventInterval) {
        // This is on FALLING, so CLK will always be 0
        if (digitalRead(volDet) == 1) {
            volumeUp();
        } else {
            volumeDown();
        }
    }
    lastVolEvt = millis();
}

/**
 * Scroll up and down based on rotary encoder
 */
void updateScroll() {
    if (millis() - lastScrollEvt > eventInterval) {
        // This is on FALLING, so CLK will always be 0
        if (digitalRead(scrollDet) == 1) {
            Serial.println("Calling scrollDown");
            scrollDown();
        } else {
            Serial.println("Calling scrollUp");
            scrollUp();
        }
    }
    lastScrollEvt = millis();
}

void setup() {
    pinMode(ledPin, OUTPUT);

    pinMode(0, INPUT_PULLUP);  // top button
    pinMode(1, INPUT_PULLUP);  // bottom left button
    pinMode(2, INPUT_PULLUP);  // bottom middle button
    pinMode(3, INPUT_PULLUP);  // bottom right button

    // Setup rotary encoder 1 (middle)
    pinMode(4, INPUT_PULLUP);  // middle rotary button
    pinMode(scrollClk, INPUT_PULLUP);  // clk (left)
    pinMode(scrollDet, INPUT_PULLUP);  // det (right)
    attachInterrupt(digitalPinToInterrupt(scrollClk), updateScroll, FALLING);
    attachInterrupt(digitalPinToInterrupt(4), undo, FALLING);

    // Setup rotary encoder 2 (right)
    pinMode(9, INPUT_PULLUP);  // right rotary button
    pinMode(volClk, INPUT_PULLUP);  // clk (left)
    pinMode(volDet, INPUT_PULLUP);  // det (right)
    attachInterrupt(digitalPinToInterrupt(volClk), updateVolume, FALLING);
    attachInterrupt(digitalPinToInterrupt(9), toggleMute, FALLING);

    Serial.begin(9600);
    Serial.println("Serial ready");

    Keyboard.begin();
}

void loop() {
    if (! digitalRead(0)) {
        // Top left button
        Keyboard.press(KEY_LEFT_ALT);
        Keyboard.press('1');
        delay(150);
        Keyboard.releaseAll();
    } else if (! digitalRead(1)) {
        // Bottom left button
        /* Keyboard.press(KEY_LEFT_ALT); */
        /* Keyboard.press('3'); */
        Keyboard.press(MEDIA_PREV);
        delay(150);
        Keyboard.releaseAll();
    } else if (! digitalRead(2)) {
        // Bottom middle button
        /* Keyboard.press(KEY_LEFT_ALT); */
        /* Keyboard.press('4'); */
        Keyboard.press(MEDIA_PLAY_PAUSE);
        delay(150);
        Keyboard.releaseAll();
    } else if (! digitalRead(3)) {
        // Bottom right button
        /* Keyboard.press(KEY_LEFT_ALT); */
        /* Keyboard.press('5'); */
        Keyboard.press(MEDIA_NEXT);
        delay(150);
        Keyboard.releaseAll();
    }
}
